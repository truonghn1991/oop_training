<?php
use PHPUnit\Framework\TestCase;

require 'Rectangle.php';

class RectangleTest extends TestCase
{
    private $rectangle;

    public function testGetTypeDescription()
    {
        $result = Rectangle::getTypeDescription();
        $this->assertEquals('Type: 2', $result);
    }
}
