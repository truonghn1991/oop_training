<?php
class Shape
{
    const SHAPE_TYPE = 1;
    public $name;
    protected $lenght;
    protected $width;
    private $id;
    public $area;

    public function __construct($lenght, $width)
    {
        $this->lenght = $lenght;
        $this->width = $width;
        $this->id = uniqid();
    }

    public function setName($name)
    {
        return $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function area()
    {
        $this->area = $this->lenght * $this->width;
        return $this->area;
    }

    public static function getTypeDescription()
    {
        return 'Type: ' . self::SHAPE_TYPE;
    }

    public function getFullDescription()
    {
        return 'Shape<' . $this->id . '>: ' . $this->name . ' - ' . $this->area;
    }
}
