<?php
use PHPUnit\Framework\TestCase;

require 'Shape.php';

class ShapeTest extends TestCase
{
    private $shape;

    protected function setUp()
    {
        $this->shape = new Shape(3, 4);
    }

    protected function tearDown()
    {
        $this->shape = null;
    }

    public function testSetName()
    {
        $resultSetName = $this->shape->setName('shape');
        $this->assertEquals('shape', $resultSetName);
    }

    public function testGetName()
    {
        $resultSetName = $this->shape->setName('shape');
        $resultGetName = $this->shape->getName();

        $this->assertEquals($resultSetName, $resultGetName);
    }

    public function testArea()
    {
        $resultArea = $this->shape->area();
        $this->assertEquals(12, $resultArea);
    }

    public function testGetTypeDescription()
    {
        $result = Shape::getTypeDescription();
        $this->assertEquals('Type: 1', $result);
    }

    public function testGetFullDescription()
    {
        $name = $this->shape->setName('shape');
        $area = $this->shape->area();
        $id = $this->shape->getId();
        $result = $this->shape->getFullDescription();
        $this->assertEquals('Shape<' . $id . '>: shape - 12', $result);
    }
}
