<?php
use PHPUnit\Framework\TestCase;

require 'Shape.php';
require 'Circle.php';

class CircleTest extends TestCase
{
    private $circle;

    protected function setUp()
    {
        $this->circle = new Circle(3);
    }

    protected function tearDown()
    {
        $this->circle = null;
    }

    public function testArea()
    {
        $result = $this->circle->area();
        $this->assertEquals(3 * 3 * 3.14, $result);
    }

    public function testGetTypeDescription()
    {
        $result = Circle::getTypeDescription();
        $this->assertEquals('Type: 3', $result);
    }

    public function testGetFullDescription()
    {
        $resultCircle = 3 * 3 * 3.14;
        $name = $this->circle->setName('circle');
        $area = $this->circle->area();
        $id = $this->circle->getId();
        $result = $this->circle->getFullDescription();
        $this->assertEquals('Circle<' . $id . '>: circle - ' . $resultCircle, $result);
    }
}
