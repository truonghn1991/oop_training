<?php
class Circle extends Shape
{
    const SHAPE_TYPE = 3;
    protected $radius;
    private $id;

    public function __construct($radius)
    {
        $this->radius = $radius;
        $this->id = uniqid();
    }

    public function getId()
    {
        return $this->id;
    }

    public function area()
    {
        $this->area = 3.14 * $this->radius * $this->radius;
        return $this->area;
    }

    public static function getTypeDescription()
    {
        return 'Type: ' . self::SHAPE_TYPE;
    }

    public function getFullDescription()
    {
        return 'Circle<' . $this->id . '>: ' . $this->name . ' - ' . $this->area;
    }
}
