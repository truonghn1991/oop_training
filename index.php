<?php
include 'Rectangle.php';
include 'Circle.php';

echo Shape::getTypeDescription();

$shape = new Shape(3, 4);
$shape->setName('shape');
$shape->area();
echo $shape->getFullDescription();

echo Rectangle::getTypeDescription();

echo Circle::getTypeDescription();

$circle = new Circle(3);
$circle->area();
$circle->setName('circle');
echo $circle->getFullDescription();
